# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/widok/Desktop/newestDat7/P7-kode/lib/jsoncpp/example/stringWrite/stringWrite.cpp" "/home/widok/Desktop/newestDat7/P7-kode/build/lib/jsoncpp/example/CMakeFiles/stringWrite.dir/stringWrite/stringWrite.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_CRT_SECURE_NO_WARNINGS"
  "_GLIBCXX_USE_CXX11_ABI"
  "_SCL_SECURE_NO_WARNINGS"
  "_WIN32_WINNT=0x601"
  "_WINSOCK_DEPRECATED_NO_WARNINGS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib/jsoncpp/include"
  "../tcp/include"
  "../robot/include"
  "../communication/include"
  "../wbt-translator/include"
  "../scheduling/include"
  "../util/include"
  "../config/include"
  "../order/include"
  "/include"
  "../include"
  "../lib/jsoncpp/src/lib_json/../../include"
  "lib/jsoncpp/include/json"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/widok/Desktop/newestDat7/P7-kode/build/lib/jsoncpp/src/lib_json/CMakeFiles/jsoncpp_lib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
